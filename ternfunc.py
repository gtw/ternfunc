#!/usr/bin/env python3

from functools import reduce

bit = lambda n, i: ( n & ( 1 << i ) ) >> i
pack = lambda *l: reduce( lambda acc, inc: ( acc << 1 ) | inc, l, 0 )
extract = lambda n, *l: pack( *[ bit( n, i ) for i in l ] )

# Primitive functions
FALSE = 0b00000000
TRUE = 0b11111111
A = 0b11110000
B = 0b11001100
C = 0b10101010

# The set of linear functions
linear = [ ( A if i & 4 else 0 ) ^ ( B if i & 2 else 0 ) ^
           ( C if i & 1 else 0 ) for i in range( 8 ) ]
# Nonlinearity of each function
nonlin = [ 4 - max( [ abs( ( f ^ l ).bit_count() - 4 )
                      for l in linear ] ) for f in range( 0x100 ) ]
# Predicates for various properties of functions
affine = lambda f: not nonlin[ f ]
evasive1 = lambda f: f.bit_count() == 1
evasive2 = lambda f: ( ( evasive1( extract( f, 3, 2 ) ) or
                         evasive1( extract( f, 1, 0 ) ) ) and
                       ( evasive1( extract( f, 3, 1 ) ) or
                         evasive1( extract( f, 2, 0 ) ) ) )
evasive = lambda f: ( ( evasive2( extract( f, 7, 6, 5, 4 ) ) or
                        evasive2( extract( f, 3, 2, 1, 0 ) ) ) and
                      ( evasive2( extract( f, 7, 6, 3, 2 ) ) or
                        evasive2( extract( f, 5, 4, 1, 0 ) ) ) and
                      ( evasive2( extract( f, 7, 5, 3, 1 ) ) or
                        evasive2( extract( f, 6, 4, 2, 0 ) ) ) )
monotonic = lambda f: ( not ( ( f & ~A ) & ~( f & A ) >> 4  ) and
                        not ( ( f & ~B ) & ~( f & B ) >> 2  ) and
                        not ( ( f & ~C ) & ~( f & C ) >> 1  ) )
prestrue = lambda f: bool( bit( f, 7 ) )
presfalse = lambda f: not bit( f, 0 )
selfdual = lambda f: ( f & 0x0F ^ ( ( ( f & 0x80 ) >> 7 ) |
                                    ( ( f & 0x40 ) >> 5 ) |
                                    ( ( f & 0x20 ) >> 3 ) |
                                    ( ( f & 0x10 ) >> 1 ) ) ) == 0x0F
semibent = lambda f: nonlin[ f ] == 2
sufficient = lambda f: ( not monotonic( f ) and not affine( f ) and
                         not selfdual( f ) and not prestrue( f ) and
                         not presfalse( f ) )
symmetric = lambda f: ( bit( f, 1 ) == bit( f, 2 ) and
                        bit( f, 1 ) == bit( f, 4 ) and
                        bit( f, 3 ) == bit( f, 5 ) and
                        bit( f, 3 ) == bit( f, 6 ) )

class Node: # an arbitrary Boolean expression
    def __init__( self, weight, pri, pretty, val, sym=False, neg=False ):
        self.weight = weight # expression complexity
        self.pri = pri # operator precedence
        self.pretty = pretty # TeX notation
        self.val = val # function enumeration
        self.sym = sym # expression makes symmetry explicit
        self.neg = neg # node is a negation of some other expression
        if symmetric( val ) and not sym: self.weight += 0x10000
    
    def paren( self, pri ): # parenthesise if necessary
        return self.pretty if self.pri <= pri else "(" + self.pretty + ")"

    def consider( self ): # remember function if better than any already known
        if not funcs[ self.val ] or funcs[ self.val ].weight > self.weight:
            funcs[ self.val ] = self
            return True

sym = lambda x, y, z: ( x.val == A and y.val == B and z.val == C ) or \
    ( x.val == A & B and y.val == A & C and z.val == B & C ) \
    if z else x.sym and y.sym
        
def Primitive( x, val, weight ): # constant 0/1 or variable A/B/C
    return Node( weight, 0, x, val, symmetric( val ) )

def LogicalNot( x ): # /X
    return Node( x.weight + ( 0x02 if x.pri < 1 else 0x030 ), 1,
                 "\\overline{" + x.pretty + "}", ~x.val & TRUE, x.sym, True )
        
def LogicalAnd( x, y, z = None ): # XY[Z]
    return Node( x.weight + y.weight + 0x080 + ( 0x20 + z.weight if z else 0 ),
                 2, x.paren( 2 ) + "\\," + y.paren( 2 ) +
                 ( "\\," + z.paren( 2 ) if z else "" ),
                 x.val & y.val & ( z.val if z else TRUE ), sym( x, y, z ) )
        
def LogicalXor( x, y, z = None ): # X XOR Y [XOR Z]
    return Node( x.weight + y.weight + 0x2FF + ( 0x40 + z.weight if z else 0 ) +
                 ( 0x100 if x.neg or y.neg or ( z and z.neg ) else 0 ), 3,
                 x.paren( 3 ) + "\\oplus{}" + y.paren( 3 ) +
                 ( ( "\\oplus{}" + z.paren( 3 ) ) if z else "" ),
                 x.val ^ y.val ^ ( z.val if z else 0x00 ), sym( x, y, z ) )
        
def LogicalOr( x, y, z = None ): # X+Y[+Z]
    return Node( x.weight + y.weight + 0x084 + ( 0x30 + z.weight if z else 0 ),
                 4, x.paren( 4 ) + "+" + y.paren( 4 ) +
                 ( ( "+" + z.paren( 4 ) ) if z else "" ),
                 x.val | y.val | ( z.val if z else 0x00 ), sym( x, y, z ) )
        
def LogicalImpl( x, y ): # X->Y
    return Node( x.weight + y.weight + 0x085, 5,
                 x.paren( 5 ) + "\\rightarrow{}" + y.paren( 5 ),
                 ( ~x.val | y.val ) & TRUE )
        
def LogicalEquiv( x, y, z = None ): # X=Y[=Z]
    return Node( x.weight + y.weight + 0x300 + ( 0x40 + z.weight if z else 0 ),
                 6, x.paren( 5 ) + "\\equiv{}" + y.paren( 5 ) +
                 ( ( "\\equiv{}" + z.paren( 5 ) ) if z else "" ),
                 ( x.val & y.val & ( z.val if z else TRUE ) ) |
                 ( ~x.val & ~y.val & TRUE & ( ~z.val if z else TRUE ) ),
                 sym( x, y, z ) )

funcs = [ None ] * 0x100

Primitive( '0', FALSE, 0 ).consider()
Primitive( '1', TRUE, 0 ).consider()
Primitive( 'A', A, 0x108 ).consider()
Primitive( 'B', B, 0x104 ).consider()
Primitive( 'C', C, 0x100 ).consider()

finished = False
while not finished:
    finished = True
    avail = sorted( [ f for f in funcs if f and f.weight ],
                    key=lambda f: f.weight )

    for i in range( len( avail ) ):
        if LogicalNot( avail[ i ] ).consider(): finished = False

        for j in range( i + 1, len( avail ) ):
            ops = sorted( [ avail[ i ], avail[ j ] ], key=lambda f: -f.weight )
            for op in [ LogicalAnd, LogicalOr, LogicalXor, LogicalEquiv,
                        LogicalImpl ]:
                if op( *ops ).consider(): finished = False

            if avail[ i ].weight < 0x300 and avail[ j ].weight < 0x300:
                for k in range( len( avail ) ):
                    ops = sorted( [ avail[ i ], avail[ j ], avail[ k ] ],
                                  key=lambda f: -f.weight )
                    for op in [ LogicalAnd, LogicalOr, LogicalXor,
                                LogicalEquiv ]:
                        if op( *ops ).consider(): finished = False

print( """\\documentclass{article}
\\usepackage[landscape,margin=1cm,top=4cm]{geometry}
\\usepackage{graphicx,xcolor,tabularray}
\\UseTblrLibrary{diagbox}
\\pagestyle{empty}
\\begin{document}""" )
print( """\\begin{center}
{\\Huge \\bf \\color{red} THE TERNARY BOOLEAN FUNCTIONS} \\par
\\vspace{5mm}
\\scalebox{0.4}{\\begin{tblr}{hlines,vlines,columns={c,m},row{1}={yellow},column{1}={yellow}}\\diagbox{MS}{LS}""" )

perm = [ 0, 1, 2, 4, 8, 9, 5, 3, 12, 10, 6, 7, 11, 13, 14, 15 ]

for c in perm:
    print( "& { {\\bf", "\\_{:X}".format( c ), "} \\\\", "\\_\\_\\_\\_{:04b}".format( c ), "}" )
print( "\\\\" )

for r in perm:
    print( "{ {\\bf", "{:X}\\_".format( r ), "} \\\\", "{:04b}\\_\\_\\_\\_".format( r ), "}" )
    for c in perm:
        bal = ( ( r << 4 ) | c ).bit_count()
        print( "&{\\SetCell{bg=" + ( "red!" + str( 40 - 10 * bal ) if bal < 4 else "green!" + str( 10 * bal - 40 ) ) + "!white}$" + funcs[ ( r << 4 ) | c ].pretty + "$\\\\" )
        for col, pred in [ ( 'red', affine ), ( 'orange', evasive ), ( 'yellow', monotonic ), ( 'green', selfdual ), ( 'cyan', semibent ), ( 'violet', symmetric ) ]:
            print( "\\color{" + col + "}\\rule{3mm}{" + ( "3" if pred( ( r << 4 ) | c ) else "0" ) + "mm} " )
        print( "}" )
    print( "\\\\" )
           
print( """\\end{tblr}}
\\vspace{3mm} \\par
\\scalebox{0.8}{\\begin{tblr}{column{odd}={r,wd=3cm},column{even}={l,wd=6cm},vline{3}={wd=1cm,white},row{odd}={lightgray},row{1}={yellow}}
\\SetCell[c=2]{c} { \\bf Operators (by decreasing precedence): } & &
\\SetCell[c=2]{c} { \\bf Function properties: } \\\\
$\\overline{A}$ & NOT (negation) & \\color{red}\\rule{2.5mm}{2.5mm} & Affine \\\\
$AB$ & AND (conjunction) & \\color{orange}\\rule{2.5mm}{2.5mm} & Evasive \\\\
$A\\oplus{}B$ & XOR (exclusive disjunction) & \\color{yellow}\\rule{2.5mm}{2.5mm} & Monotonic\\\\
$A+B$ & OR (inclusive disjunction) & \\color{green}\\rule{2.5mm}{2.5mm} & Self-dual \\\\
$A\\rightarrow{}B$ & implication (conditional) & \\color{cyan}\\rule{2.5mm}{2.5mm} & Semi-bent\\\\
$A\\equiv{}B$ & equivalence (biconditional) & \\color{violet}\\rule{2.5mm}{2.5mm} & Symmetric \\\\
& & \\colorbox{red!40!white}{\\makebox[3cm]{False imbalance}}\\makebox[3cm]{Balanced}\\colorbox{green!40!white}{\\makebox[3cm]{True imbalance}} \\\\
\\end{tblr}}
\\end{center}\\newpage\\begin{center}
{\\Huge \\bf \\color{red}FUNCTIONAL COMPLETENESS} \\par
{\\bf \\color{red}OF THE TERNARY BOOLEAN FUNCTIONS} \\par
\\vspace{5mm}
\\scalebox{0.4}{\\begin{tblr}{hlines,vlines,columns={c,m},row{1}={yellow},column{1}={yellow}}\\diagbox{MS}{LS}""" )

permc = [ 0, 8, 4, 12, 2, 10, 6, 14, 1, 9, 5, 13, 3, 11, 7, 15 ]

for c in permc:
    print( "& { {\\bf", "\\_{:X}".format( c ), "} \\\\", "\\_\\_\\_\\_{:04b}".format( c ), "}" )
print( "\\\\" )

for r in range( 16 ):
    print( "{ {\\bf", "{:X}\\_".format( r ), "} \\\\", "{:04b}\\_\\_\\_\\_".format( r ), "}" )
    for c in permc:
        print( "&" + ( "" if sufficient( ( r << 4 ) | c ) else "\\SetCell{bg=lightgray}" ) + "{$" + funcs[ ( r << 4 ) | c ].pretty + "$\\\\" )

        for col, pred in [ ( 'red', presfalse ), ( 'cyan', affine ), ( 'orange', selfdual ), ( 'green', prestrue ), ( 'yellow', monotonic ) ]:
            print( "\\color{" + col + "}\\rule{3mm}{" + ( "3" if pred( ( r << 4 ) | c ) else "0" ) + "mm} " )
        print( "}" )
    print( "\\\\" )
           
print( """\\end{tblr}}
\\vspace{3mm} \\par
\\scalebox{0.8}{\\begin{tblr}{column{odd}={r,wd=3cm},column{even}={l,wd=6cm},vline{3}={wd=1cm,white},cell{odd}{1-2}={lightgray},cell{2-6}{3-4}={lightgray},row{1}={yellow}}
\\SetCell[c=2]{c} { \\bf Operators (by decreasing precedence): } & &
\\SetCell[c=2]{c} { \\bf Post's classes: } \\\\
$\\overline{A}$ & NOT (negation) & \\color{yellow}\\rule{2.5mm}{2.5mm} & $M$ (monotonic) \\\\
$AB$ & AND (conjunction) & \\color{orange}\\rule{2.5mm}{2.5mm} & $D$ (self-dual) \\\\
$A\\oplus{}B$ & XOR (exclusive disjunction) & \\color{cyan}\\rule{2.5mm}{2.5mm} & $A$ (affine)\\\\
$A+B$ & OR (inclusive disjunction) & \\color{red}\\rule{2.5mm}{2.5mm} & $P_0$ (false-preserving) \\\\
$A\\rightarrow{}B$ & implication (conditional) & \\color{green}\\rule{2.5mm}{2.5mm} & $P_1$ (true-preserving) \\\\
$A\\equiv{}B$ & equivalence (biconditional) & & Sole sufficient \\\\
\\end{tblr}}
\\end{center}
\\end{document}""" );
