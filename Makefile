all: ternfunc.pdf

ternfunc.pdf: ternfunc.tex
	lualatex ternfunc

ternfunc.tex: ternfunc.py
	python3 $< > $@
